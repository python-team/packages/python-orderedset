Source: python-orderedset
Maintainer: Roland Mas <lolando@debian.org>
Section: python
Priority: optional
Build-Depends: dh-python, python3-setuptools, python3-all-dev,
 debhelper-compat (= 13), cython3-legacy
Standards-Version: 4.5.1
Homepage: https://github.com/simonpercivall/orderedset

Package: python3-orderedset
Architecture: any
Depends: ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends}
Description: Ordered Set implementation in Cython.
 An Ordered Set implementation in Cython.
 .
 Features:
 .
  * Works like a regular set, but remembers insertion order;
  * Is approximately 5 times faster than the pure Python implementation overall
    (and 5 times slower than `set`);
  * Compatible with Python 2.7 through 3.8;
  * Supports the full set interface;
  * Supports some list methods, like `index` and `__getitem__`.
  * Supports set methods against iterables.
